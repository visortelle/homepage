---
author: thoughtpolice
title: "GHC Weekly News - 2015/02/23"
date: 2015-02-23
tags: ghc news
---

Hi \*,

It's once again time for your sometimes-slightly-irregularly-scheduled GHC news! This past Friday marked the end of the FTP vote for GHC 7.10, there's an RC on the way (see below), we've closed out a good set of patches and tickets from users and pushed them into `HEAD`, and to top it off - it's your editor's birthday today, so that's nice!

Quick note: as said above GHC HQ is expecting to make a **third** release candidate for GHC 7.10.1 soon in early March since the delay has allowed us to pick up some more changes and bugfixes. We plan on the final release being close to the end of March (previously end of February).

This week, GHC HQ met up again to discuss and write down our current priorities and thoughts:

 - After discussing our current timetable - as we're currently hovering around the ICFP deadline - we're hoping to make our third GHC 7.10.1 release candidate on **Friday, March 13th**, with the final release on **Friday, March 27th**. This was the main take away from our meeting today.

We've also had a little more list activity this week than we did before:

 - The FTP debate has ended, and the results are in: GHC 7.10.1 will continue with the generalized Prelude, known as "Plan FTP". <https://mail.haskell.org/pipermail/libraries/2015-February/025009.html>

 - Edward Kmett announced the `directory` package needed an active maintainer to step up - and luckily, Phil Ruffwind and Elliot Robinson did just that and stepped up as maintainers! <https://mail.haskell.org/pipermail/ghc-devs/2015-February/008358.html>

 - Kazu Yamamoto asked about a behavioral change in `ghc-7.10` for `Data.Char` - it turns out this difference looks like it's caused by GHC 7.10 shipping an update to use Unicode 7.0 datasets. <https://mail.haskell.org/pipermail/ghc-devs/2015-February/008371.html>

 - Thomas Bereknyei asked about a fundamental change in the Arrow desugarer, and whether or not something like this was worth it. Jan Stolarek and Ross Paterson stepped in to speak up to some specifics Thomas had about. <https://mail.haskell.org/pipermail/ghc-devs/2015-February/008377.html>

 - Gabor Grief spoke up about strange behavior in the desugarer when using `RebindableSyntax` and `RankNTypes`, which Adam Gundry popped in to say may be a deeper issue due to the way typechecking and desugaring interact - <https://mail.haskell.org/pipermail/ghc-devs/2015-February/008383.html>

 - Johan Tibell announced Cabal 1.22.1.0, which will ship with GHC 7.10. <https://mail.haskell.org/pipermail/ghc-devs/2015-February/008388.html>

Some noteworthy commits that went into `ghc.git` in the past week include:

  - Commit 1b82619bc2ff36341d916c56b0cd67a378a9c222 - The `hpc` commands now take a configurable verbosity level (merged to `ghc-7.10)

  - Commit 0fa20726b0587530712677e50a56c2b03ba43095 - GHC now errors out on a module explicitly declared `Main` without a `main` export.

Closed tickets the past week include: #9266, #10095, #9959, #10086, #9094, #9606, #9402, #10093, #9054, #10102, #4366, #7604, #9103, #10104, #7765, #7103, #10051, #7056, #9907, #10078, #10096, #10072, #10043, #9926, #10088, #10091, #8309, #9049, #9895, and #8539.
